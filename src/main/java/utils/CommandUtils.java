package utils;

import application.Constants;
import enums.Commands;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static enums.Commands.*;

public class CommandUtils {

    public static boolean isVerificationTrigger(Message message) {
        final String rawMessage = message.getContentRaw().toLowerCase();
        final Guild guild = message.getGuild();
        final Member selfMember = guild.getSelfMember();
        Role botRole = guild.getRoleByBot(selfMember.getId());

        return (message.getMentionedUsers().stream()
                .anyMatch(u -> u.getId().equalsIgnoreCase(selfMember.getId())) ||
                message.getMentionedRoles().stream()
                        .anyMatch(r -> botRole != null && r.getId().equalsIgnoreCase(botRole.getId()))) &&
                (rawMessage.contains(Constants.VERIFICATION_TRIGGER_WORD));
    }

    public static boolean isCommand(Message message)
    {
        return Arrays.stream(Commands.values()).anyMatch(val -> message.getContentRaw().toLowerCase().startsWith( val.value.toLowerCase() )) ||
                isVerificationTrigger(message) || isExitTrigger(message);
    }

    public static boolean isControlCommand(Message message)
    {
        String lowerCaseMessage = message.getContentRaw().toLowerCase();

        return lowerCaseMessage.startsWith(SET_USER_AWAY_NOTIFICATION_HOURS.value) || lowerCaseMessage.startsWith(SET_USER_ROLES_AUTO_REVOKE_HOURS.value) ||
                lowerCaseMessage.startsWith(REMOVE_CMD.value) || lowerCaseMessage.startsWith(ADD_CMD.value) || lowerCaseMessage.startsWith(HELP_CMD.value);
    }

    public static boolean isExitTrigger(Message message) {
        final Guild guild = message.getGuild();
        final Member selfMember = guild.getSelfMember();
        Role botRole = guild.getRoleByBot(selfMember.getId());

        final String rawMessage = message.getContentRaw().toLowerCase();
        return (message.getMentionedUsers().stream()
                .anyMatch(u -> u.getId().equalsIgnoreCase(guild.getSelfMember().getId())) ||
                        message.getMentionedRoles().stream()
                                .anyMatch(r -> botRole != null && r.getId().equalsIgnoreCase(botRole.getId()))) &&
                (rawMessage.contains(Constants.EXIT_TRIGGER_WORD));
    }

    public static boolean canExecutePrivilegedCommand(Member member, Map<String, List<String>> rolesMap) {
        return member.hasPermission(Permission.ADMINISTRATOR) ||
                GuildRolesUtils.memberHasRoles(member, rolesMap.get("contactRoleIds")) ||
                rolesMap.get("contactUserIds").contains(member.getId());
    }
}
