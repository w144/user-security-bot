package utils;

import entities.User;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GuildRolesUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(GuildRolesUtils.class);

    public static void removeSensitiveAndSosUserRoles(String userId, Guild guild, Map<String, List<String>> rolesMap) {
        List<String> sensitiveRoleNames = rolesMap.get("sensitiveRoleNames");
        List<String> sosRoleNames = rolesMap.get("sosRoleNames");

        LOGGER.info("#### Removing roles... ");

        for (Role role : guild.getRoles()) {
            final String roleName = role.getName();

            if (sensitiveRoleNames != null && sensitiveRoleNames.contains(roleName) || sosRoleNames != null && sosRoleNames.contains(roleName)) {
                guild.removeRoleFromMember(userId, role).queue();
            }
        }
    }

    public static void assignUserRoles(User user, Guild guild, List<String> roleNames) {
        StringBuilder rolesToLog = new StringBuilder("#### Assigning roles ");

        for (Role role : guild.getRoles()) {
            String roleName = role.getName();

            if (roleNames != null && roleNames.contains(roleName) && user.hasRole(roleName)) {
                guild.addRoleToMember(user.getUserId(), role).queue();
                rolesToLog.append(roleName).append(", ");
            }
        }

        LOGGER.info(rolesToLog.toString());
    }

    public static void assignSosUserRoles(String userId, Guild guild, List<String> roleNames) {
        LOGGER.info("#### Assigning SOS roles to user " + userId + ": " + StringUtils.join(roleNames, ","));
        for (Role role : guild.getRoles()) {
            if (roleNames!= null && roleNames.contains(role.getName())) {
                guild.addRoleToMember(userId, role).queue();
            }
        }
    }

    public static boolean memberHasRoles(Member member, List<String> roleIds)
    {
        return member.getRoles().stream().map(Role::getId).collect(Collectors.toList()).removeAll(roleIds);
    }
}
