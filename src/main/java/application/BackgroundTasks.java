package application;

import entities.User;
import enums.CurrState;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.GuildRolesUtils;

import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static net.dv8tion.jda.api.requests.ErrorResponse.UNKNOWN_MEMBER;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static utils.CommonUtils.sendAlert;
import static utils.CommonUtils.tryParseInt;

public class BackgroundTasks {
    private static final Logger LOGGER = LoggerFactory.getLogger(BackgroundTasks.class);
    private static final int MILLIS_IN_HOUR = 1000 * 3600;

    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);
    private final Map<String, User> userMap;
    private final Map<String, List<String>> rolesMap;
    private final Map<String, String> commonSettingsMap;
    private final JDA jda;

    public BackgroundTasks(Map<String, User> userMap, Map<String, List<String>> rolesMap, Map<String, String> commonSettingsMap, JDA jda) {
        this.userMap = userMap;
        this.rolesMap = rolesMap;
        this.commonSettingsMap = commonSettingsMap;
        this.jda = jda;
    }

    public void run() {
        Guild guild = jda.getGuildById(commonSettingsMap.get("guildId"));

        if (guild == null)
        {
            LOGGER.error("guild {} is null.", commonSettingsMap.get("guildId"));
            return;
        }

        Runnable serviceInfoLoggingTask = () -> {
            LOGGER.debug("******** Service status info section ********");
            LOGGER.debug("**** Total bot users: {}", userMap.size());
            LOGGER.debug("**** Total guild members: {}", guild.getMemberCount());
            LOGGER.debug("**** Gateway ping: {}", jda.getGatewayPing());
            LOGGER.debug("**** JDA status: {}", jda.getStatus());
            LOGGER.debug("**** Memory total: {}, free: {} KB", Runtime.getRuntime().totalMemory() / 1024, Runtime.getRuntime().freeMemory() / 1024);
            LOGGER.debug("**** Uptime {} hours", String.format("%.2f",ManagementFactory.getRuntimeMXBean().getUptime() / 1000.0 / 3600));

            for (User user : userMap.values()) {
                final String userId = user.getUserId();

                try {
                    Member member = guild.retrieveMemberById(userId).complete();

                    if (member == null) {
                        LOGGER.warn("**** Member {} is null", userId);
                        continue;
                    }

                    if (CurrState.GOOD_PASSWORD.equals(user.getState()) || isBlank(user.getGoodPwd())) {
                        LOGGER.warn("**** User {} has registration pending", member.getEffectiveName());
                    }
                    else if (CurrState.USER_IS_AWAY.equals(user.getState())) {
                        LOGGER.warn("**** User {} is away. Last seen online {}.", member.getEffectiveName(), user.getLastSeenOnline());
                    }
                }
                catch (ErrorResponseException e)
                {
                    if (UNKNOWN_MEMBER.equals(e.getErrorResponse()))
                    {
                        userMap.remove(userId);

                        LOGGER.warn("**** Unknown user {} removed from the DB", userId);
                    }
                    else
                    {
                        LOGGER.warn("**** ErrorResponseException occurred while retrieving user {}", userId, e);
                    }
                }
                catch (Exception e)
                {
                    LOGGER.warn("**** Exception occurred while retrieving user {}", userId, e);
                }
            }
        };

        Runnable userAwayNotificationTask = () -> {
            int userAwayNotificationHours = tryParseInt(commonSettingsMap.get("userAwayNotificationHours"), 0);

            if (CollectionUtils.isEmpty(rolesMap.get("contactRoleIds")) && CollectionUtils.isEmpty(rolesMap.get("contactUserIds")) || userAwayNotificationHours <= 0)
            {
                return;
            }

            Date currentDate = new Date();

            for( User user : userMap.values() )
            {
                final String userId = user.getUserId();
                final Date lastMessageDate = user.getLastMessageDate();

                if (CurrState.USER_IS_AWAY.equals(user.getState()) && isNotBlank(user.getGoodPwd()) && !user.isAwayNotificationSent() && lastMessageDate != null &&
                         (currentDate.getTime() - lastMessageDate.getTime()) > userAwayNotificationHours * MILLIS_IN_HOUR) {
                    sendAlert(guild, "**Внимание!**\r\nПользователь <@" + userId + "> " +
                            "не отбился в течении " + (userAwayNotificationHours == 1 ? "1 часа!" : userAwayNotificationHours + " часов!") +
                            "\r\n**Последнее сообщение:** " + user.getLastMessage() +
                            "\r\n**Время:** " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(user.getLastMessageDate()), userId, rolesMap, Constants.ALERT_CHANNEL_NAME_NO_RESPONSE);
                    user.setUserAwayNotificationSent(true);
                    userMap.put(userId, user);

                    guild.retrieveMemberById(userId).queue(member -> LOGGER.warn("#### User {} didn't respond within {} hours. Alert sent!", (member != null ? member.getEffectiveName() : userId), userAwayNotificationHours)
                    );
                }
            }
        };

        Runnable inactiveUserRolesAutoRevokeTask = () -> {
            int inactiveUserRolesAutoRevokeHours = tryParseInt(commonSettingsMap.get("inactiveUserRolesAutoRevokeHours"), 0);

            if (inactiveUserRolesAutoRevokeHours <= 0)
            {
                return;
            }

            final Date currentDate = new Date();

            for( User user : userMap.values() )
            {
                final String userId = user.getUserId();
                final Date lastSeenOnline = user.getLastSeenOnline();

                if(CurrState.IDLE.equals(user.getState()) && lastSeenOnline != null &&
                        (currentDate.getTime() - lastSeenOnline.getTime()) > inactiveUserRolesAutoRevokeHours * MILLIS_IN_HOUR)
                {
                    guild.retrieveMemberById(userId).queue(member -> {
                        if (member.getOnlineStatus().equals(OnlineStatus.OFFLINE))
                        {
                            user.setUserRoleNames(member.getRoles().stream().map(Role::getName).collect(Collectors.toList()));
                            LOGGER.info("#### Revoking roles from inactive user {}", member.getEffectiveName());
                            GuildRolesUtils.removeSensitiveAndSosUserRoles(userId, guild, rolesMap);
                            user.setState(CurrState.USER_IS_AWAY);
                            user.setLastMessageDate(currentDate);
                            user.setLastMessage("<Роли пользователя были удалены автоматически, так как он был оффлайн в течение " +
                                    (inactiveUserRolesAutoRevokeHours == 1 ? "1 часа" : inactiveUserRolesAutoRevokeHours + " часов") + ">");
                            user.setUserAwayNotificationSent(false);
                            userMap.put(userId, user);
                        }
                    });
                }
            }
        };

        Runnable syncManuallyAddedOrRemovedUsersStateTask = () -> {
            final List<String> sensitiveRoleNames = rolesMap.get("sensitiveRoleNames");

            for (User user : userMap.values()) {
                final String userId = user.getUserId();

                if (CurrState.USER_IS_AWAY.equals(user.getState())) {
                    try
                    {
                        Member member = guild.retrieveMemberById(userId).complete();

                        if (member == null)
                        {
                            continue;
                        }

                        if (member.getRoles().stream().map(Role::getName).anyMatch(sensitiveRoleNames::contains)) {
                            LOGGER.info("#### User state sync task. Checking {} (USER_IS_AWAY) roles...", member.getEffectiveName());
                            // There might be a situation when roles had been removed normally by user's exit request, but haven't applied or updated in the cache yet.
                            // Waiting some time then checking again.
                            Thread.sleep(30000);

                            member = guild.retrieveMemberById(userId).complete();

                            //If still true - need to update user state: the associated member has at least one secure role - put the user to IDLE state.
                            if (member.getRoles().stream().map(Role::getName).anyMatch(sensitiveRoleNames::contains)) {
                                user.setState(CurrState.IDLE);
                                userMap.put(userId, user);
                                LOGGER.info("#### Moving user {} to IDLE state because they already have secure roles.", member.getEffectiveName());
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.warn("#### User state sync task. Exception message: {}", e.getMessage());
                    }
                }
                else if (CurrState.IDLE.equals(user.getState()) && CollectionUtils.isNotEmpty(user.getUserRoles()))
                {
                    try {
                        Member member = guild.retrieveMemberById(userId).complete();

                        if (member == null)
                        {
                            continue;
                        }

                        if (member.getRoles().stream().map(Role::getName).noneMatch(sensitiveRoleNames::contains))
                        {
                            LOGGER.info("#### User state sync task. Checking {} (IDLE) roles...", member.getEffectiveName());
                            // There might be a situation when roles had been assigned normally after user successful verification, but haven't applied or updated in the cache yet.
                            // Waiting some time then checking again.
                            Thread.sleep(30000);

                            member = guild.retrieveMemberById(userId).complete();

                            // If still true - need to update user state: the associated member doesn't have any secure roles but the user has saved roles and is in IDLE state - put them to USER_IS_AWAY state.
                            if (member.getRoles().stream().map(Role::getName).noneMatch(sensitiveRoleNames::contains)) {
                                user.setState(CurrState.USER_IS_AWAY);
                                userMap.put(userId, user);
                                LOGGER.info("#### Moving user {} to USER_IS_AWAY state because they have no secure roles.", member.getEffectiveName());
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        LOGGER.warn("#### User state sync task. Exception caught :{}", e.getMessage());
                    }
                }
            }
        };

        executor.scheduleWithFixedDelay(serviceInfoLoggingTask, 0, 12, TimeUnit.HOURS);
        executor.scheduleWithFixedDelay(userAwayNotificationTask, 1, 10, TimeUnit.MINUTES);

        if (jda.getGatewayIntents().containsAll(EnumSet.of(GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_PRESENCES)))
        {
            //This will not work without privileged intents
            executor.scheduleWithFixedDelay(inactiveUserRolesAutoRevokeTask, 2, 10, TimeUnit.MINUTES);
        }
        executor.scheduleWithFixedDelay(syncManuallyAddedOrRemovedUsersStateTask, 3, 30, TimeUnit.MINUTES);
    }
}
