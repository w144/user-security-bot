package entities;

import enums.CurrState;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class User implements Serializable {
    public static final long serialVersionUID = -8733238144788161756L;

    private long privateChannelId;
    private String userId;
    private CurrState state;
    private List<String> userRoles;
    private String goodPwd;
    private String sosPwd;
    private String lastMessage;
    private Date lastMessageDate;
    private boolean userAwayNotificationSent;
    private Date lastSeenOnline;

    public CurrState getState() {
        return state;
    }

    public void setState(CurrState state) {
        this.state = state;
    }

    public User(long privateChannelId, CurrState state) {
        this.privateChannelId = privateChannelId;
        this.state = state;
    }

    public long getPrivateChannelId() {
        return privateChannelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserRoleNames(List<String> userRoles) {
        this.userRoles = userRoles;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPrivateChannelId(long privateChannelId) {
        this.privateChannelId = privateChannelId;
    }

    public String getGoodPwd() {
        return goodPwd;
    }

    public void setGoodPwd(String goodPwd) {
        this.goodPwd = goodPwd;
    }

    public String getSosPwd() {
        return sosPwd;
    }

    public void setSosPwd(String sosPwd) {
        this.sosPwd = sosPwd;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public boolean isAwayNotificationSent() {
        return userAwayNotificationSent;
    }

    public void setUserAwayNotificationSent(boolean userAwayNotificationSent) {
        this.userAwayNotificationSent = userAwayNotificationSent;
    }

    public Date getLastSeenOnline() {
        return lastSeenOnline;
    }

    public void setLastSeenOnline(Date lastSeenOnline) {
        this.lastSeenOnline = lastSeenOnline;
    }

    public List<String> getUserRoles() {
        return userRoles;
    }

    public boolean hasRole(String name) {
        return !CollectionUtils.isEmpty(userRoles) && userRoles.contains(name);
    }
}
