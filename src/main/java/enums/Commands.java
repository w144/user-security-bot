package enums;

public enum Commands {

    START_CMD("uc!start", "регистрация пользователя (настройка паролей для верификации)."),
    REMOVE_CMD("uc!remove", "@user, ...@user_n - поместить пользователей (не обязательно зареганных) в general-only роль."),
    ADD_CMD("uc!add", "@user, ...@user_n - вернуть пользователям секьюрные роли."),
    SET_USER_AWAY_NOTIFICATION_HOURS("uc!set_user_away_notification_hours", "задать время в часах через которое будет отправлено уведомление что пользователь не отбился."),
    SET_USER_ROLES_AUTO_REVOKE_HOURS("uc!set_user_roles_auto_revoke_hours", "задать время в часах через которое у неактивного пользователя будут удалены секьюрные роли."),
    SETUP_CMD("uc!setup", "настройка бота админом (указание ролей сервера для управления доступом)."),
    HELP_CMD("uc!help", "Описание комманд.");

    public final String value;
    public final String descrition;

    Commands(String value, String descrition) {
        this.value = value;
        this.descrition = descrition;
    }


}
