package application;

import entities.User;
import enums.Commands;
import enums.CurrState;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.api.events.user.update.UserUpdateOnlineStatusEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.h2.mvstore.MVStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.CommandUtils;
import utils.CommonUtils;
import utils.GuildRolesUtils;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static application.Constants.EXIT_TRIGGER_WORD;
import static application.Constants.VERIFICATION_TRIGGER_WORD;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static utils.CommonUtils.isFromTempChannels;
import static utils.CommonUtils.tryParseInt;

public class Main extends ListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static MVStore appDataStore;

    private static Map<String, entities.User> userMap;
    private static Map<String, List<String>> rolesMap;
    private static Map<String, String> commonSettingsMap;

    public static void main(String args[]) throws LoginException, IllegalAccessException, NoSuchFieldException {
        System.setProperty("file.encoding", "UTF-8");
        Field charset = Charset.class.getDeclaredField("defaultCharset");
        charset.setAccessible(true);
        charset.set(null, null);

        String botToken;

        try {
            botToken = Files.lines(Paths.get("bot_token")).findFirst().orElseThrow(() -> new IOException("bot_token file is empty!")).trim();
        } catch (IOException e) {
            LOGGER.error("#### Failed to read bot token. Make sure that file 'bot_token' exists in bot directory. Impossible to continue.", e);
            return;
        }

        JDABuilder jdaBuilder = JDABuilder.create(botToken, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_PRESENCES);
        jdaBuilder.addEventListeners(new Main()).setMemberCachePolicy(MemberCachePolicy.ALL).enableCache(CacheFlag.CLIENT_STATUS);
        JDA jda;

        try {
            jda = jdaBuilder.build();
        } catch ( IllegalArgumentException e)
        {
            LOGGER.error("#### Failed to initialize JDA with privileged intents, trying again in limited mode...");
            jda = JDABuilder.create(botToken, GatewayIntent.GUILD_MESSAGES).addEventListeners(new Main()).build();
        }

        jda.getPresence().setActivity(Activity.listening(Commands.HELP_CMD.value));

        appDataStore = MVStore.open("storage.dat");
        userMap = appDataStore.openMap("userMap");
        rolesMap = appDataStore.openMap("rolesMap");
        commonSettingsMap = appDataStore.openMap("commonSettingsMap");

        LOGGER.debug("#### Loaded {} users.", userMap.size());
    }

    @Override
    public void onReady(@Nonnull ReadyEvent event) {
        LOGGER.info("Guild available count = {} ", event.getGuildAvailableCount());

        BackgroundTasks backgroundTasks = new BackgroundTasks(userMap, rolesMap, commonSettingsMap, event.getJDA());

        backgroundTasks.run();
    }

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent messageReceivedEvent) {
        dispatchAndProcess(messageReceivedEvent.getMessage());
    }

    @Override
    public void onGuildMessageUpdate(@Nonnull GuildMessageUpdateEvent guildMessageUpdateEvent) {
        dispatchAndProcess(guildMessageUpdateEvent.getMessage());
    }

    @Override
    public void onUserUpdateOnlineStatus(@Nonnull UserUpdateOnlineStatusEvent event) {
        Member member = event.getMember();

        if( !event.getOldOnlineStatus().equals(OnlineStatus.OFFLINE) && event.getNewOnlineStatus().equals(OnlineStatus.OFFLINE) )
        {
            User user = userMap.get(member.getId());
            //track only registered users
            if (user != null && isNotBlank(user.getGoodPwd()))
            {
                user.setLastSeenOnline(new Date());
                userMap.put(member.getId(), user);
            }
        }
    }

    private void dispatchAndProcess(@Nonnull Message message) {
        final String rawMessage = message.getContentRaw();

        try {
            if (rawMessage.toLowerCase().contains("жыве беларусь"))
            {
                message.addReaction("U+1F90D").queue();
                message.addReaction("U+2764").queue();
                message.addReaction("U+1F49F").queue();
            }

            if (rawMessage.toLowerCase().contains("котятки") || rawMessage.toLowerCase().contains(" котики") || rawMessage.toLowerCase().contains("котоны"))
            {
                message.addReaction("U+1F63B").queue();
            }
        } catch (Exception e)
        {
            LOGGER.error("#### :(");
        }

        if (!message.isFromType(ChannelType.TEXT) || message.getAuthor().isBot() || CommandUtils.isCommand(message) == isFromTempChannels(message))
        {
            return;
        }

        final Guild guild = message.getGuild();
        final String userId = message.getAuthor().getId();
        final entities.User currentUser = userMap.get(userId);
        final Member member = message.getMember();

        if (member==null)
        {
            LOGGER.warn("#### member is null, raw message: " + rawMessage);
            return;
        }

        if (CommandUtils.isControlCommand(message))
        {
            executeControlCommand(message);
            return;
        }

        LOGGER.info("#### Received event from user: {}", message.getAuthor().getName());

        if (CommandUtils.isExitTrigger(message) && currentUser != null && !member.hasPermission(Permission.ADMINISTRATOR))
        {
            if (StringUtils.isBlank(currentUser.getGoodPwd()))
            {
                currentUser.setState(CurrState.REGISTRATION);
            }
            else
            {
                LOGGER.info("#### User {} is quiting [{}], message: {}", message.getAuthor().getName(), StringUtils.join(member.getActiveClients(), ", "), rawMessage);

                try {
                    if (!CurrState.USER_IS_AWAY.equals(currentUser.getState()))
                    {
                        currentUser.setUserRoleNames(member.getRoles().stream().map(Role::getName).collect(Collectors.toList()));
                        GuildRolesUtils.removeSensitiveAndSosUserRoles(userId, guild, rolesMap);
                        currentUser.setState(CurrState.USER_IS_AWAY);
                    }
                    currentUser.setLastMessage(rawMessage);
                    currentUser.setLastMessageDate(new Date());
                    currentUser.setUserAwayNotificationSent(false);
                    userMap.put(userId, currentUser);
                } catch (HierarchyException e) {
                    LOGGER.error("#### Failed to override permissions", e);
                    message.getChannel().sendMessage("Внимание! Произошла ошибка! Нужно убедиться что роль бота настроена с наивысшим приоритетом.").queue(CommonUtils::deleteMsgAfter15Sec);
                }

                CommonUtils.deleteMsgAfter15Sec(message);

                return;
            }
        }

        CurrState currState;

        if (rawMessage.equalsIgnoreCase(Commands.SETUP_CMD.value)) {

            if ( member.hasPermission(Permission.ADMINISTRATOR) )
            {
                currState = CurrState.ADMIN_SETUP;
            }
            else
            {
                message.reply("Эта команда доступна только админам!").queue(CommonUtils::deleteMsgAfter15Sec);
                CommonUtils.deleteMsgAfter15Sec(message);
                return;
            }

        } else if (currentUser == null || rawMessage.equalsIgnoreCase(Commands.START_CMD.value) && CommonUtils.userCanRegister(currentUser) ) {
            currState = CurrState.REGISTRATION;
        } else if (CommandUtils.isVerificationTrigger(message) ) {
            currState = CurrState.VERIFICATION;
        } else {
            currState = currentUser.getState();
        }

        switch (currState) {
            case ADMIN_SETUP:
                TextChannel channel = CommonUtils.createTempPrivateChannel(member, guild.getSelfMember(), Constants.SETUP_CHANNEL_NAME);
                CommonUtils.deleteMsgAfter15Sec(message);
                channel.sendMessage("Привет <@" + userId + ">!\r\n" +
                        "Пожалуйста перечислите через @ роли которые должны быть удалены у пользователя для безопасности\r\n" +
                        "Критические роли можно удалять/возвращать у пользователей командами uc!remove/uc!add и упоминанием их через @.").queue();
                userMap.put(userId, new entities.User(channel.getIdLong(), CurrState.SETUP_SENSITIVE_ROLES));

                break;
            case SETUP_SENSITIVE_ROLES:
                channel = CommonUtils.getCurrentUserChannel(message, currentUser);

                if (CollectionUtils.isEmpty(message.getMentionedRoles()))
                {
                    channel.sendMessage("Укажите хотя бы одну роль (через @)").queue();
                    currentUser.setState(CurrState.SETUP_SENSITIVE_ROLES);
                    break;
                }

                rolesMap.put("sensitiveRoleNames", message.getMentionedRoles().stream().map(Role::getName).collect(Collectors.toList()));
                channel.sendMessage("Пожалуйста перечислите через @ роли которые должны быть назначены пользователю в случае опасности.\r\n" +
                        "Либо ввидите любой символ чтобы пропустить шаг.").queue();
                currentUser.setState(CurrState.SETUP_SOS_ROLES);

                break;
            case SETUP_SOS_ROLES:
                channel = CommonUtils.getCurrentUserChannel(message, currentUser);
                rolesMap.put("sosRoleNames", message.getMentionedRoles().stream().map(Role::getName).collect(Collectors.toList()) );
                channel.sendMessage("Пожалуйста перечислите через @ роли или пользователей, которым будет отправлятся сигнал о тревоге.\r\n" +
                        "Либо ввидите любой символ чтобы пропустить шаг.").queue();
                currentUser.setState(CurrState.SETUP_CONTACT_ROLES);
                break;

            case SETUP_CONTACT_ROLES:
                rolesMap.put("contactRoleIds", message.getMentionedRoles().stream().map(Role::getId).collect(Collectors.toList()));
                rolesMap.put("contactUserIds", message.getMentionedMembers().stream().map(ISnowflake::getId).collect(Collectors.toList()));
                channel = CommonUtils.getCurrentUserChannel(message, currentUser);

                if( !CollectionUtils.isEmpty(rolesMap.get("contactRoleIds")) || !CollectionUtils.isEmpty(rolesMap.get("contactUserIds")) )
                {
                    channel.sendMessage("Задайте время в часах, через которое будет отправлен сигнал о тревоге если пользователь не \"отбился\".\r\n" +
                            "По умолчанию - 12 часов.").queue();
                    currentUser.setState(CurrState.SETUP_USER_ABSENT_TIMEOUT);
                }
                else
                {
                    channel.sendMessage("Настройка успешно завершена!").queue();
                    LOGGER.info("#### admin setup completed by user " + member.getEffectiveName());

                    userMap.remove(userId);
                    commonSettingsMap.put("guildId", guild.getId());
                    appDataStore.commit();
                }
                break;
            case SETUP_USER_ABSENT_TIMEOUT:
                channel = CommonUtils.getCurrentUserChannel(message, currentUser);
                commonSettingsMap.put("userAwayNotificationHours", String.valueOf(tryParseInt(rawMessage, 12)));
                channel.sendMessage("Настройка успешно завершена!").queue();
                LOGGER.info("#### admin setup completed by user " + member.getEffectiveName());
                channel.delete().queueAfter(Constants.REGISTRATION_CHANNEL_TIME_TO_LIVE_SEC, TimeUnit.SECONDS);

                userMap.remove(userId);
                commonSettingsMap.put("guildId", guild.getId());
                appDataStore.commit();

                break;
            case REGISTRATION:
                LOGGER.info("#### user registration initiated by " + member.getEffectiveName());
                CommonUtils.deleteMsgAfter15Sec(message);

                if(!CommonUtils.setupComplete(rolesMap))
                {
                    message.reply("Бот пока не настроен. Вызовите команду " + Commands.SETUP_CMD.value + " если вы админ.").queue(CommonUtils::deleteMsgAfter15Sec);
                    break;
                }

                channel = CommonUtils.createTempPrivateChannel(member, guild.getSelfMember(), Constants.VERIFICATION_CHANNEL_NAME);

                if (member.hasPermission(Permission.ADMINISTRATOR))
                {
                    channel.sendMessage("Привет <@" + userId + ">!\r\n Ваш админский аккаунт не может быть защищен.\r\n Для безопасности заведите отдельный обычный аккаунт.").queue();
                    channel.delete().queueAfter(Constants.REGISTRATION_CHANNEL_TIME_TO_LIVE_SEC, TimeUnit.SECONDS);
                    break;
                }

                channel.sendMessage("Привет <@" + userId + ">!\r\n" + Constants.BOT_INTRO_MESSAGE).queue();
                userMap.put(userId, new User(channel.getIdLong(), CurrState.GOOD_PASSWORD));

                break;
            case GOOD_PASSWORD:
                currentUser.setGoodPwd(CommonUtils.generateSha256Hash(rawMessage.toLowerCase()));
                message.getChannel().sendMessage("Пожалуйста задайте SOS-пароль:").queue();

                currentUser.setState(CurrState.SOS_PASSWORD);

                break;
            case SOS_PASSWORD:
                channel = CommonUtils.getCurrentUserChannel(message, currentUser);

                String sosPasswordHash = CommonUtils.generateSha256Hash(rawMessage.toLowerCase());

                if (currentUser.getGoodPwd().equals(sosPasswordHash))
                {
                    channel.sendMessage("Пароли не должны быть одинаковыми, придумайте другой пароль.").queue();
                    currentUser.setState(CurrState.SOS_PASSWORD);
                }
                else
                {
                    LOGGER.info("#### registration completed by " + member.getEffectiveName());
                    channel.sendMessage(Constants.REG_COMPLETED_MESSAGE).queue();
                    currentUser.setUserId(userId);
                    currentUser.setSosPwd(sosPasswordHash);
                    currentUser.setState(CurrState.IDLE);
                    userMap.put(userId, currentUser);
                    appDataStore.commit();
                    channel.delete().queueAfter(Constants.REGISTRATION_CHANNEL_TIME_TO_LIVE_SEC, TimeUnit.SECONDS);
                }

                break;
            case VERIFICATION:
                if (!currentUser.getState().equals(CurrState.USER_IS_AWAY))
                {
                    LOGGER.info("#### Attempting to initiate verification in {} state by user {}", currentUser.getState(), member.getEffectiveName());

                    if(currentUser.getState().equals(CurrState.IDLE))
                    {
                        message.reply("Для начала нужно выйти, написав " + "<@" + guild.getSelfMember().getId() + ">, " + EXIT_TRIGGER_WORD + " <дополнительная инфа>").queue(CommonUtils::deleteMsgAfter15Sec);
                        CommonUtils.deleteMsgAfter15Sec(message);
                    }

                    return;
                }

                LOGGER.info("#### verification initiated by user " + member.getEffectiveName());
                CommonUtils.deleteMsgAfter15Sec(message);
                channel = CommonUtils.createTempPrivateChannel(member, guild.getSelfMember(), Constants.VERIFICATION_CHANNEL_NAME);

                channel.sendMessage("<@" + userId + ">, введите пароль:").queue();
                currentUser.setPrivateChannelId(channel.getIdLong());
                currentUser.setState(CurrState.VALIDATION);
                break;
            case VALIDATION:
                if( !isFromTempChannels(message) )
                {
                    return;
                }

                String msg;

                final String caseInsensitivePwdHash = CommonUtils.generateSha256Hash(rawMessage.toLowerCase());
                final String userGoodPwdHash = currentUser.getGoodPwd();

                if (caseInsensitivePwdHash.equals(userGoodPwdHash)) {
                    LOGGER.info("#### password accepted for user {}", member.getEffectiveName());
                    GuildRolesUtils.assignUserRoles(currentUser, guild, rolesMap.get("sensitiveRoleNames"));
                    currentUser.setState(CurrState.IDLE);
                    userMap.put(userId, currentUser);
                    msg = "Пароль принят!";
                } else if (caseInsensitivePwdHash.equals(currentUser.getSosPwd())) {
                    GuildRolesUtils.assignSosUserRoles(userId, guild, rolesMap.get("sosRoleNames"));

                    msg = "Пароль принят!";

                    CommonUtils.sendAlert(guild, "**Внимание!**\r\nПользователь <@" + userId + "> ввёл пароль под давлением!" +
                            "\r\n**Последнее сообщение:** " + currentUser.getLastMessage() + "\r\n**Время:** " +
                            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentUser.getLastMessageDate()), userId, rolesMap, Constants.ALERT_CHANNEL_NAME_IN_TROUBLE);

                    LOGGER.warn("#### User {} entered password under pressure! Alert sent!", member.getEffectiveName());

                    currentUser.setState(CurrState.IDLE_IN_TROUBLE);
                    userMap.put(userId, currentUser);
                } else {
                    msg = "Неверный пароль!";
                    currentUser.setState(CurrState.USER_IS_AWAY);
                    LOGGER.warn("#### user " + member.getEffectiveName() + " entered incorrect password!");
                }

                channel = CommonUtils.getCurrentUserChannel(message, currentUser);
                channel.sendMessage(msg).queue();

                channel.delete().queueAfter(Constants.VERIFICATION_CHANNEL_TIME_TO_LIVE_SEC, TimeUnit.SECONDS);
        }
    }

    private void executeControlCommand(@Nonnull Message message) {
        final Member member = message.getMember();
        final String rawMessage = message.getContentRaw().toLowerCase();
        final Guild guild = message.getGuild();

        if (rawMessage.equals(Commands.HELP_CMD.value)) {
            StringBuilder helpMessage = new StringBuilder();

            for (Commands cmd: Commands.values())
            {
                helpMessage.append(cmd.value).append(" - ").append(cmd.descrition).append("\r\n");
            }

            helpMessage.append("\r\n")
                    .append("<@").append(guild.getSelfMember().getId()).append(">, " + EXIT_TRIGGER_WORD + " <предполагаемое время возврата или другая важная инфа> - вводится пользователем для перемещения в general-only роль.\r\n")
                    .append("<@").append(guild.getSelfMember().getId()).append(">, фраза содержащая \"" + VERIFICATION_TRIGGER_WORD + "\" - вводится пользователем для начала верификации.");

            message.reply(helpMessage.toString()).queue();
            return;
        }

        if (!CommandUtils.canExecutePrivilegedCommand(member, rolesMap)) {
            message.reply("Эта команда вам недоступна!").queue(CommonUtils::deleteMsgAfter15Sec);
            return;
        }

        //Privileged commands go here (only Admins and contact users can execute them)

        if (rawMessage.startsWith(Commands.REMOVE_CMD.value) || rawMessage.startsWith(Commands.ADD_CMD.value)) {
            if (!CommonUtils.setupComplete(rolesMap)) {
                message.reply("Бот пока не настроен. Вызовите команду " + Commands.SETUP_CMD.value + " если вы админ.").queue(CommonUtils::deleteMsgAfter15Sec);

                CommonUtils.deleteMsgAfter15Sec(message);
                return;
            }

            try {
                handleAddRemove(message);

                message.reply("Сделано!").queue(CommonUtils::deleteMsgAfter15Sec);

            } catch (HierarchyException e) {
                LOGGER.error("Failed to override permissions", e);
                message.reply("Внимание! Произошла ошибка! Убедитесь что пользователи не обладают админскими правами.").queue(CommonUtils::deleteMsgAfter15Sec);
            }
            CommonUtils.deleteMsgAfter15Sec(message);
        }
        else if (rawMessage.startsWith(Commands.SET_USER_AWAY_NOTIFICATION_HOURS.value))
        {
            try {
                int userAwayNotificationHours = Integer.parseInt(rawMessage.split(" ")[1]);

                if (userAwayNotificationHours < 0)
                {
                    message.reply("Пожалуйста задайте положительное число через пробел после комманды.").queue(CommonUtils::deleteMsgAfter15Sec);
                }
                else
                {
                    commonSettingsMap.put("userAwayNotificationHours", String.valueOf(userAwayNotificationHours));
                    message.reply("Значение установлено!").queue(CommonUtils::deleteMsgAfter15Sec);
                    LOGGER.info("#### userAwayNotificationHours value has been set to {}", commonSettingsMap.get("userAwayNotificationHours"));
                }
            }
            catch (Exception e)
            {
                message.reply("Пожалуйста задайте целое положительное число или 0 (для отключения) через пробел после комманды.").queue(CommonUtils::deleteMsgAfter15Sec);
            }
            CommonUtils.deleteMsgAfter15Sec(message);
        }
        else if (rawMessage.startsWith(Commands.SET_USER_ROLES_AUTO_REVOKE_HOURS.value))
        {
            try {
                int inactiveUserRolesAutoRevokeHours = Integer.parseInt(rawMessage.split(" ")[1]);

                if (inactiveUserRolesAutoRevokeHours < 0)
                {
                    message.reply("Пожалуйста задайте положительное число через пробел после комманды.").queue(CommonUtils::deleteMsgAfter15Sec);
                }
                else
                {
                    commonSettingsMap.put("inactiveUserRolesAutoRevokeHours", String.valueOf(inactiveUserRolesAutoRevokeHours));
                    message.reply("Значение установлено!").queue(CommonUtils::deleteMsgAfter15Sec);
                    LOGGER.info("#### userAwayNotificationHours value has been set to {}", commonSettingsMap.get("userAwayNotificationHours"));
                }
            }
            catch (Exception e)
            {
                message.reply("Пожалуйста задайте целое положительное число или 0 (для отключения) через пробел после комманды.").queue(CommonUtils::deleteMsgAfter15Sec);
            }
            CommonUtils.deleteMsgAfter15Sec(message);
        }
    }

    private void handleAddRemove(Message message) throws HierarchyException {
        final String rawMessage = message.getContentRaw().toLowerCase();
        final Guild guild = message.getGuild();

        LOGGER.info("#### Handle add/remove {}, initiated by {}. Members:", rawMessage, message.getAuthor().getName());

        for (Member mentionedMember : message.getMentionedMembers()) {
            String memberId = mentionedMember.getId();

            LOGGER.info("#### {}", mentionedMember.getEffectiveName());

            if (rawMessage.startsWith(Commands.REMOVE_CMD.value.toLowerCase())) {

                if (mentionedMember.hasPermission(Permission.ADMINISTRATOR)) {
                    message.reply("Пользователь <@" + memberId + "> не может быть защищён!").queue(CommonUtils::deleteMsgAfter15Sec);
                    LOGGER.warn("#### Cannot remove admin member {} !", mentionedMember.getEffectiveName());
                    continue;
                }

                final List<String> memberRoleNames = mentionedMember.getRoles().stream().map(Role::getName).collect(Collectors.toList());

                if (CollectionUtils.isNotEmpty(memberRoleNames))
                {
                    User tempUser = userMap.get(memberId);

                    if (tempUser == null)
                    {
                       tempUser = new User(-1, CurrState.IDLE);
                    }

                    if (CurrState.USER_IS_AWAY.equals(tempUser.getState()))
                    {
                        message.reply("Пользователь <@" + memberId + "> уже был удалён.").queue(CommonUtils::deleteMsgAfter15Sec);
                        continue;
                    }

                    tempUser.setState(CurrState.USER_IS_AWAY);
                    tempUser.setUserId(memberId);
                    tempUser.setUserRoleNames(memberRoleNames);

                    GuildRolesUtils.removeSensitiveAndSosUserRoles(memberId, guild, rolesMap);

                    userMap.put(memberId, tempUser);
                }
                else
                {
                    message.reply("У пользователя <@" + memberId + "> уже отсутствуют какие-либо роли.").queue(CommonUtils::deleteMsgAfter15Sec);
                }
            } else if (rawMessage.startsWith(Commands.ADD_CMD.value.toLowerCase())) {
                User existingUser = userMap.get(memberId);

                if ( existingUser != null && CollectionUtils.isNotEmpty(existingUser.getUserRoles()) && !CurrState.IDLE.equals(existingUser.getState()) )
                {
                    GuildRolesUtils.assignUserRoles(existingUser, guild, rolesMap.get("sensitiveRoleNames"));

                    existingUser.setState(CurrState.IDLE);
                    userMap.put(memberId, existingUser);
                }
                else
                {
                    message.reply("Пользователь <@" + memberId + "> не был удалён через бота. Сначала придётся назначить нужные роди вручную.").queue(CommonUtils::deleteMsgAfter15Sec);
                }
            }
        }
    }

}
