package application;

public interface Constants
{
  String VERIFICATION_TRIGGER_WORD = "привет";
  String EXIT_TRIGGER_WORD = "я выхожу";

  String ALERT_CHANNEL_NAME_NO_RESPONSE = "Человек не отбился";
  String ALERT_CHANNEL_NAME_IN_TROUBLE = "Человек в опасности";

  String VERIFICATION_CHANNEL_NAME = "проверка";
  String SETUP_CHANNEL_NAME = "настройка";

  int REGISTRATION_CHANNEL_TIME_TO_LIVE_SEC = 30;
  int VERIFICATION_CHANNEL_TIME_TO_LIVE_SEC = 15;

  String PASSWORD_HASH_SALT = "Nzk0NjU2MjE1NTk3ODQyNDgyWGhsgSAo2qUF9caW_RBZICgohu4";

  String BOT_INTRO_MESSAGE =
          "Этот бот поможет защитить сервер от доступа нехороших людей.\r\n" +
          "При вводе обычного пароля вам будут возвращены роли для доступа к секретным каналам\r\n" +
          "При вводе SOS-пароля (под давлением), внешне будет выглядеть как будто вы ввели нормальный пароль, но без доступа к настоящим секретным каналам.\r\n\r\n" +
          "Пожалуйста задайте свой обычный пароль:";

  String REG_COMPLETED_MESSAGE =
    "Регистрация прошла успешно!\r\n" +
        "Перед выходом на акцию, либо в другой ситуации с риском, напишите в general канал сообщение \"" + EXIT_TRIGGER_WORD + " <предполагаемое время возврата или другая важная инфа> с обращением к боту.\"\r\n." +
        "Для смены паролей введите команду uc!start \r\n" +
        "Для верификации - фразу содержащую \"" + VERIFICATION_TRIGGER_WORD + "\" с обращением к боту.";

}
