package utils;

import application.Constants;
import entities.User;
import enums.CurrState;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.ChannelAction;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

public class CommonUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtils.class);

    public static boolean isFromTempChannels(Message message) {
        return message.getChannel().getName().startsWith(Constants.VERIFICATION_CHANNEL_NAME) ||
                message.getChannel().getName().startsWith(Constants.SETUP_CHANNEL_NAME);
    }

    public static boolean userCanRegister(User user)
    {
        return !(user.getState().equals(CurrState.VERIFICATION) || user.getState().equals(CurrState.VALIDATION) || user.getState().equals(CurrState.USER_IS_AWAY) || user.getState().equals(CurrState.IDLE_IN_TROUBLE));
    }

    public static TextChannel getCurrentUserChannel(@Nonnull Message message, User currentUser) {
        return message.getJDA().getTextChannelById(currentUser.getPrivateChannelId());
    }

    public static boolean setupComplete(Map<String, List<String>> rolesMap) {
        return !CollectionUtils.isEmpty( rolesMap.get("sensitiveRoleNames") );
    }

    public static void deleteMsgAfter15Sec(Message message )
    {
        message.delete().queueAfter(15, TimeUnit.SECONDS);
    }

    public static void sendAlert(Guild guild, String alertMessage, String currentUserId, Map<String, List<String>> rolesMap, String channelName)
    {
        List<TextChannel> existingAlertChannels = guild.getTextChannelsByName(channelName.replace(" ", "-"), true);
        TextChannel existingAlertChannel = CollectionUtils.isNotEmpty(existingAlertChannels) ? existingAlertChannels.get(0) : null;

        ChannelAction<TextChannel> channelAction = null;

        if(existingAlertChannel == null)
        {
            channelAction = guild.createTextChannel(channelName).setPosition(0)
                .addPermissionOverride(guild.getPublicRole(), null, EnumSet.of(Permission.VIEW_CHANNEL));
            LOGGER.warn("#### Sending alert (new channel).");
        }
        else
        {
            LOGGER.warn("#### Sending alert (existing channel).");
        }

        StringBuilder mentions = new StringBuilder();

        for(String id : rolesMap.get("contactRoleIds"))
        {
            final Role role = guild.getRoleById(id);

            if(role == null)
            {
                LOGGER.warn("#### Contact role {} doesn't exist.", id);
                continue;
            }

            if(existingAlertChannel != null)
            {
                existingAlertChannel.putPermissionOverride(role).setAllow(EnumSet.of(Permission.VIEW_CHANNEL, Permission.MANAGE_CHANNEL)).resetDeny().queue();
            }
            else
            {
                channelAction = channelAction.addPermissionOverride(role, EnumSet.of(Permission.VIEW_CHANNEL, Permission.MANAGE_CHANNEL), null);
            }

            mentions.append("<@&").append(id).append(">, ");
        }

        for(String id : rolesMap.get("contactUserIds"))
        {
            Member member = guild.getMemberById(id);

            if(member == null)
            {
                LOGGER.warn("#### Contact member {} doesn't exist.", id);
                continue;
            }

            //don't notify user when they are unsafe
            if(!id.equalsIgnoreCase(currentUserId))
            {
                if(existingAlertChannel != null)
                {
                    existingAlertChannel.putPermissionOverride(member).setAllow(EnumSet.of(Permission.VIEW_CHANNEL, Permission.MANAGE_CHANNEL)).resetDeny().queue();
                }
                else
                {
                    channelAction = channelAction.addPermissionOverride(member, EnumSet.of(Permission.VIEW_CHANNEL, Permission.MANAGE_CHANNEL), null);
                }
            }
            else
            {
                LOGGER.warn("#### Skipping alert to unsafe user {}", currentUserId);
            }

            mentions.append("<@!").append(id).append(">, ");
        }

        TextChannel alertChannel;

        if(existingAlertChannel != null)
        {
            existingAlertChannel.putPermissionOverride(guild.getSelfMember()).setAllow(EnumSet.of(Permission.ADMINISTRATOR)).complete();
            alertChannel = existingAlertChannel;
        }
        else
        {
            alertChannel = channelAction.addPermissionOverride(guild.getSelfMember(), EnumSet.of(Permission.ADMINISTRATOR), null).complete();
        }

        alertChannel.sendMessage(mentions.append("\r\n").append(alertMessage).toString()).queue();
    }

    public static int tryParseInt(String value, int defaultValue)
    {
        try
        {
            return Integer.parseInt(value);
        }
        catch (NumberFormatException e)
        {
            return defaultValue;
        }
    }

    public static String generateSha256Hash(String rawPassword) {
        return DigestUtils.sha256Hex(rawPassword.concat(Constants.PASSWORD_HASH_SALT));
    }

    public static TextChannel createTempPrivateChannel(Member member, Member self, String name) {
        Guild guild = member.getGuild();
        TextChannel channel = guild.createTextChannel(name)
                .addPermissionOverride(guild.getPublicRole(), null, EnumSet.of(Permission.VIEW_CHANNEL))
                .addPermissionOverride(member, EnumSet.of(Permission.VIEW_CHANNEL, Permission.MANAGE_CHANNEL), null)
                .addPermissionOverride(self, EnumSet.of(Permission.ADMINISTRATOR), null)
                .complete();
        //always delete abandoned channels after 10 minutes
        channel.delete().queueAfter(10, TimeUnit.MINUTES, RestAction.getDefaultSuccess(), RestAction.getDefaultSuccess() );

        return channel;
    }
}
